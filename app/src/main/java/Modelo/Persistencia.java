package Modelo;

import com.example.preexamenc3.Usuarios;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuarios usuario);
}
