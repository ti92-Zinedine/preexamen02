package com.example.preexamenc3;

public class Usuarios {
    private int ID;
    private String nombre;
    private String correo;
    private String contrasenia;

    public Usuarios(){
        this.ID = 0;
        this.nombre = "";
        this.correo = "";
        this.contrasenia = "";
    }

    public Usuarios(int id, String nombre, String correo, String contrasenia){
        this.ID = id;
        this.nombre = nombre;
        this.correo = correo;
        this.contrasenia = contrasenia;
    }

    //Encapsulamiento
    public int getIdUsuario() {
        return ID;
    }

    public void setIdUsuario(int id) {
        this.ID = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
}
