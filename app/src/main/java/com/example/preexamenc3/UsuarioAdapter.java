package com.example.preexamenc3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class UsuarioAdapter extends BaseAdapter {
    Context context;
    List<Usuarios> lista;
    LayoutInflater inflater;

    public UsuarioAdapter(Context context, List<Usuarios> lista){
        this.lista = lista;
        this.context = context;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int posicion, View convertView, ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.empresas_items, null);
        }

        TextView txtNombre = (TextView) convertView.findViewById(R.id.lblNombre);
        txtNombre.setText(lista.get(posicion).getNombre());

        TextView txtCorreo = (TextView) convertView.findViewById(R.id.lblCorreo);
        txtCorreo.setText(lista.get(posicion).getCorreo());

        return convertView;
    }
}
