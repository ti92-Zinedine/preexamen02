package com.example.preexamenc3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Modelo.UsuarioDb;

public class MainActivity extends AppCompatActivity {
    private EditText txtCorreo;
    private EditText txtPassword;
    private Button btnRegistrar;
    private Button btnIngresar;
    private Button btnSalir;
    static UsuarioDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCorreo = findViewById(R.id.txtCorreo);
        txtPassword = findViewById(R.id.txtContrasena);
        btnRegistrar = findViewById(R.id.btnRegistrarse);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);

        usuarioDb = new UsuarioDb(getApplicationContext());
        usuarioDb.openDataBase();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void registrar() {
        Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
        startActivity(intent);
    }

    private void ingresar() {
        String correo = txtCorreo.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        usuarioDb = new UsuarioDb(getApplicationContext());

        boolean siRegistrado = usuarioDb.getUsuario(correo, password);
        usuarioDb.openDataBase();

        if(siRegistrado){
            txtCorreo.setText("");
            txtPassword.setText("");
            Intent intent = new Intent(MainActivity.this, ListaActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Correo o Contraseña no válidos", Toast.LENGTH_LONG).show();
        }
    }

    private void salir() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Empresas Industriales de México");
        dialogo.setMessage("¿Desea salir de la app?");

        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }
}